# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=nymphcast
pkgver=0_git20200309
pkgrel=0
_commit="d5adc8921e3bd357ce8715122d8bb17ca4bd547e"
arch="all !armv7" # Assembly failure
url="http://nyanko.ws/product_nymphcast.php"
pkgdesc="Audio and video casting system with support for custom applications"
license="BSD-3-Clause"
depends="avahi"
depends_dev="$pkgname-sdk"
makedepends="nymphrpc nymphrpc-dev sdl2-dev sdl2_image-dev ffmpeg-dev openssl-dev"
source="$pkgname-$_commit.tar.gz::https://github.com/MayaPosch/NymphCast/archive/$_commit.tar.gz
	nymphcast.initd"
subpackages="$pkgname-sdk $pkgname-sdk-static:sdk_static $pkgname-dev $pkgname-openrc"
options="!check" # No tests
builddir="$srcdir/NymphCast-$_commit"

build() {
	make -C "$builddir"/src/server
	make -C "$builddir"/src/client_lib
}

package() {
	# Install the server
	cd "$builddir"/src/server

	install -Dm755 bin/nymphcast_server "$pkgdir"/usr/bin/nymphcast_server
	for i in *.ini; do
		install -Dm644 "$i" "$pkgdir"/etc/nymphcast/"$i"
	done

	install -d \
		"$pkgdir"/usr/share/nymphcast
	cp -r apps "$pkgdir"/usr/share/nymphcast/
	for file in *.jpg; do
		install -Dm644 "$file" "$pkgdir"/usr/share/nymphcast/wallpapers/$file
	done

	install -Dm755 "$srcdir"/nymphcast.initd "$pkgdir"/etc/init.d/nymphcast

	install -Dm644 avahi/nymphcast.service "$pkgdir"/etc/avahi/services/nymphcast.service

	# Install the client SDK header
	cd "$builddir"/src/client_lib
	install -Dm644 nymphcast_client.h "$pkgdir"/usr/include/nymphcast_client.h
}

sdk() {
	pkgdesc="$pkgdesc (SDK)"

	cd "$builddir"/src/client_lib
	install -Dm644 lib/libnymphcast.so "$subpkgdir"/usr/lib/libnymphcast.so
}

sdk_static() {
	pkgdesc="$pkgdesc (static SDK)"

	cd "$builddir"/src/client_lib
	install -Dm644 lib/libnymphcast.a "$subpkgdir"/usr/lib/libnymphcast.a
}

sha512sums="2a48b771a0c11760891013a38fd60406edf6a6d7cdd3c38ca6dbe98851aeb933a3a673deebcb7081e68d50a43a0b22d60c90323d18754decf5767b614a3439b1  nymphcast-d5adc8921e3bd357ce8715122d8bb17ca4bd547e.tar.gz
1ab3f0dad11bca5790a2a637bc624c6b80990d09990a9f5ef3a4537c9c04abf40924633c571ffa86758886d5f21253bb5d0369b17b01f90dcc39b3e609caa405  nymphcast.initd"
